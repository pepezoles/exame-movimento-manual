package br.com.exame.movimentomanual.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class ProdutoCosifId implements Serializable {	

	private static final long serialVersionUID = 1L;

	@Column(name = "COD_PRODUTO", nullable = false)
	private Integer produtoId;
	
	@Column(name = "COD_COSIF", nullable = false)
	private String produtoCosifId;
	
}
