package br.com.exame.movimentomanual.model;

import lombok.Data;

@Data
public class ProdutoDTO {
	private Integer produtoId;
	private String descricao;
	private String status;
}
