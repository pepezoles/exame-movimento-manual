package br.com.exame.movimentomanual.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.exame.movimentomanual.model.MovimentoManualDTO;
import br.com.exame.movimentomanual.model.facade.MovimentoManualFacade;

@Component
public class IncluirMovimentoManual {
	
	@Autowired
	private MovimentoManualFacade facade;
	
	public void execute(MovimentoManualDTO dto) {
		
	}	

}
