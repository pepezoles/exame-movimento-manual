package br.com.exame.movimentomanual.model;

import lombok.Data;

@Data
public class ProdutoCosifDTO {
	
	private Integer produtoId;
	private Integer produtoCosifId;
	private String classificacao;
	private String status;
	
}
