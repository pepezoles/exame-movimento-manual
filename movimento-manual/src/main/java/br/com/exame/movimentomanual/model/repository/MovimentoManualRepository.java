package br.com.exame.movimentomanual.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.exame.movimentomanual.model.entity.MovimentoManual;
import br.com.exame.movimentomanual.model.entity.MovimentoManualId;

@Repository
public interface MovimentoManualRepository extends JpaRepository<MovimentoManual, MovimentoManualId> {

}
