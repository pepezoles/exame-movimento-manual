package br.com.exame.movimentomanual.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class MovimentoManualId implements Serializable {	

	private static final long serialVersionUID = 1L;

	@Column(name = "DAT_MES")
	private Integer mes;
	
	@Column(name = "DAT_ANO")
	private Integer ano;
	
	@Column(name = "NUM_LANCAMENTO")
	private Integer numeroLancamento;
	
	@Column(name = "COD_PRODUTO")
	private Integer produtoId;
	
	@Column(name = "COD_COSIF")
	private Integer produtoCosifId;
	
}
