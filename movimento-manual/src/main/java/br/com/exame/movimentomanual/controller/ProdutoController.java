package br.com.exame.movimentomanual.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.exame.movimentomanual.model.ProdutoDTO;
import br.com.exame.movimentomanual.usecase.FindProduto;

@RestController
public class ProdutoController {	
	
	@Autowired
	private FindProduto usecase;
	
	@GetMapping(value = "/produto")
	public ResponseEntity<Iterable<ProdutoDTO>> all() {
		return ResponseEntity.ok(usecase.findAll());
	}	
	
}


