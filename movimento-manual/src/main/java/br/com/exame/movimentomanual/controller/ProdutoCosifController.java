package br.com.exame.movimentomanual.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.exame.movimentomanual.model.ProdutoCosifDTO;
import br.com.exame.movimentomanual.usecase.FindProdutoCosif;

@RestController
public class ProdutoCosifController {	
	
	@Autowired
	private FindProdutoCosif usecase;
	
	@GetMapping(value = "/produto-cosif")
	public ResponseEntity<Iterable<ProdutoCosifDTO>> all() {
		return ResponseEntity.ok(usecase.findAll());
	}	
	
}


