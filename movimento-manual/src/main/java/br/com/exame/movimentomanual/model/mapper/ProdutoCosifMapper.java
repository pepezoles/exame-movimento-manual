package br.com.exame.movimentomanual.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import br.com.exame.movimentomanual.model.ProdutoCosifDTO;
import br.com.exame.movimentomanual.model.entity.ProdutoCosif;

@Mapper
public interface ProdutoCosifMapper {
	
	@Mappings({
		@Mapping(target = "produtoCosifId", source = "entity.id.produtoCosifId"),
		@Mapping(target = "produtoId", source = "entity.id.produtoId")})	
	ProdutoCosifDTO fromModel(ProdutoCosif entity);
}
