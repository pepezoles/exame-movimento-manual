package br.com.exame.movimentomanual.model;

import lombok.Data;

@Data
public class MovimentoManualDTO {
	private Integer produtoId;
	private Integer produtoCosifId;
	private Integer mes;
	private Integer ano;
	private Integer numeroLancamento;
	private String descricao;
	private String data;
	private Double valor;
	private String usuario;
}
