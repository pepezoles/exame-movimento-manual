package br.com.exame.movimentomanual.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import br.com.exame.movimentomanual.model.MovimentoManualDTO;
import br.com.exame.movimentomanual.model.entity.MovimentoManual;

@Mapper
public interface MovimentoManualMapper {
	
	@Mappings({
		@Mapping(target = "produtoId", source = "entity.id.produtoId"),
		@Mapping(target = "produtoCosifId", source = "entity.id.produtoCosifId"),
		@Mapping(target = "mes", source = "entity.id.mes"),
		@Mapping(target = "ano", source = "entity.id.ano"),
		@Mapping(target = "numeroLancamento", source = "entity.id.numeroLancamento"),
		@Mapping(target = "data", source = "entity.data", dateFormat = "dd/MM/yyyy HH:mm:ss")})
	MovimentoManualDTO fromModel(MovimentoManual entity);
	
	@Mappings({
		@Mapping(target = "id.produtoId", source = "produtoId"),
		@Mapping(target = "id.produtoCosifId", source = "produtoCosifId"),
		@Mapping(target = "id.mes", source = "mes"),
		@Mapping(target = "id.ano", source = "ano"),
		@Mapping(target = "id.numeroLancamento", source = "numeroLancamento"),
		@Mapping(target = "data", source = "dto.data", dateFormat = "dd/MM/yyyy HH:mm:ss")})
	MovimentoManual fromDTO(MovimentoManualDTO dto);
	
}
